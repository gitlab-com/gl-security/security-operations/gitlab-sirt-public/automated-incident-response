# GitLab Universal Automated Response and Detection (GUARD)

Welcome to the open-source repository containing detection and automation scripts developed by the GitLab Security Operations team. These scripts are designed to streamline and optimize the incident response management process, detection coverage, reduce administrative overhead, and enhance overall efficiency during incidents. By sharing these scripts, we aim to contribute to the global incident response community and empower teams to build their own Security Orchestration, Automation, and Response (SOAR) solutions tailored to their specific requirements.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Detections](#detections)
- [Contributing](#contributing)
- [License](#license)

## Introduction

Incident response involves various processes beyond initial investigations, including alert management, communication, incident tracking, handoffs, and more. These processes can often be tedious, repetitive, and time-consuming, particularly in remote work environments. The GitLab Incident Response team has developed a collection of automation scripts that primarily operate within the Slack ecosystem while integrating with critical cloud-based systems, such as GitLab, PagerDuty, SIEM, and Google Workspace. By leveraging these scripts, we have significantly optimized our incident response workflow and reduced investigation time. In addition to automation, we share the common detections that can be used by our customers using GitLab SaaS and self managed solution. Please refer to usage section for more details on how to use these detections. 

## Features

Our detections cover several areas, all of which related to a typical GitLab infrastructure. They can be found in the [detections](./detections/) folder. 

Our automation scripts cover several key functionalities of our day-to-day operations. These automations include GitLab CI/CD scrips and [Tines](https://www.tines.com/) stories. 

- **Alert Deployment**: Automate the deployment of alerts through our detection-as-code CI/CD pipeline. These can be found in the [Detection as Code](./detection-as-code/) folder. 

The rest can be found in the [Tines Stories](./tines-stories/) folder:
- **Detections in Slack**: Forward SIEM alerts to Slack with an interactive module. 
- **Team Handoffs**: Facilitate seamless handoffs between different incident response team members.
- **Token Revocation**: Makes it easy to revoke GitLab API keys. 

## Getting Started

### Prerequisites

Before using the automation scripts, ensure you have the following in place:

- Slack workspace with appropriate permissions.
- Accounts/API access to GitLab, PagerDuty, SIEM system, and Google Workspace (if applicable).
- Access to the Tines automation platform.

### Installation

The scripts apply to multiple platforms. Clone this repository to your local environment to access them:

```bash
git clone https://gitlab.com/gitlab-com/gl-security/security-operations/signals-engineering-public/guard-open-source.git
cd guard-open-source
```

## Detections

We have created a detection suite based on [GitLab's Audit Events](https://docs.gitlab.com/ee/administration/audit_event_types.html). The rules are in [Sigma](https://github.com/SigmaHQ/sigma?tab=readme-ov-file#what-is-sigma) format and can be adjusted to fit your needs. You can use tools such as [SigConverter](https://sigconverter.io/) to convert these detections to the specific query language supported by your SIEM solution. These detections were designed to protect against various types of possible attack vectors on the GitLab platform. 

The current areas of focus of our detection suite are:
- **Account Protection**: Protecting GitLab accounts from malicious activity.
- **Group Hardening**: Increasing the security and integrity of GitLab groups.
- **Project Hardening**: Increasing the security and integrity of GitLab projects.
- **Admin Activity**: Monitoring activity of admin accounts. 
- **General**: Monitoring activity at the GitLab instance level.

## Contributing

Contributions from the community to improve and expand these automation scripts will be accepted in the near future. 

## License

This project is licensed under the [MIT License](LICENSE).

---

We hope that these automation scripts prove valuable in enhancing your incident response processes. Feel free to adapt, modify, and integrate them into your workflow as needed. We look forward to your contributions and feedback!
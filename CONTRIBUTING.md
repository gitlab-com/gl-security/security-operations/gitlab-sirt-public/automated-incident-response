# Contributing to Guard

## Developer Certificate of Origin

Contributions to this repository are subject to the [Developer Certificate of Origin](https://docs.gitlab.com/legal/developer_certificate_of_origin/#developer-certificate-of-origin-version-11). By submitting any contribution to this project, you agree to be bound by these terms and certify that your contributions meet the requirements specified therein.

## Project Structure

```
.
├── detections/              # GitLab security detections in Sigma format
│   ├── account protection/  # Account-related security rules
│   ├── admin activity/     # Admin action monitoring rules
│   ├── general/           # Instance-level monitoring rules
│   ├── group hardening/   # Group security rules
│   └── project hardening/ # Project security rules
├── detection-as-code/      # CI/CD pipeline configuration for alert deployment
└── tines-stories/         # Tines automation story templates
```

## Contributing Guidelines

### General Guidelines

1. Fork the repository and create a new branch for your contribution
2. Make your changes following the guidelines below
3. Test your changes thoroughly
4. Submit a merge request with a clear description of your changes

### Contributing Detections

When adding or modifying detections:

1. All detections must be in [Sigma](https://github.com/SigmaHQ/sigma) format
2. Place the detection in the appropriate category folder under `detections/`
3. Follow the existing YAML structure and naming convention
4. Include the following in your detection:
   - Clear description of what the detection identifies
   - Relevant GitLab audit event types
   - Appropriate severity level
   - False positive scenarios (if applicable)

### Contributing Tines Stories

When adding or modifying Tines automation stories:

1. Export the story from Tines in JSON format
2. Place the story in the `tines-stories/` directory
3. Include documentation within the story:
   - Purpose and functionality
   - Required credentials/API access
   - Configuration instructions
4. Test the story thoroughly in a Tines environment
5. Remove any sensitive information (API keys, tokens, etc.)

### Code Style

- Follow YAML best practices for detection files
- Use consistent indentation (2 spaces)
- Include comments for complex logic
- Keep files focused and modular

### Testing

- For detections:
  - Test against sample audit events/GitLab logs
  - Document any false positive scenarios

- For Tines stories:
  - Test in a development environment
  - Verify all actions and triggers work as expected
  - Test error handling scenarios

### Submitting Changes

1. Create a descriptive merge request title
2. Include in your merge request description:
   - What changes were made
   - Why the changes are needed
   - How to test the changes
   - Any dependencies or prerequisites
3. Link to any relevant issues or documentation
4. Ensure CI/CD pipeline passes

## Getting Help

If you have questions or need help:

1. Check existing documentation
2. Review similar contributions
3. Open an issue in this project for any clarifications.

## License

By contributing to Guard, you agree that your contributions will be licensed under the MIT License.

---

Thank you for helping improve GitLab's security operations! Your contributions help make GitLab more secure for everyone.

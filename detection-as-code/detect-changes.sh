#!/bin/bash

if GIT_DIR=.git git rev-parse $CI_COMMIT_SHA^2 >/dev/null 2>&1
then
    echo "This is a merge commit."
    git diff-tree --no-commit-id --name-status -r $CI_COMMIT_SHA^2 | tee changed-files.txt
else
    echo "This is a standard commit."
    git diff-tree --no-commit-id --name-status -r $CI_COMMIT_SHA | tee changed-files.txt
fi

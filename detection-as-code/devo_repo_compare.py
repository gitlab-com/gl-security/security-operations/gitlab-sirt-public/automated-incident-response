import logging
import sys, os
import requests, glob, json

# For simplicity, this is set to use the local directory.
DIRECTORY = sys.path[0]
FILE_LOG = f"{DIRECTORY}/devo_repo_compare.log"
TOKEN = os.environ.get('token')
TINES_WEBHOOK = os.environ.get('tines_webhook')
HOST = 'https://api-eu.devo.com'
FOLDER = './detections'

logging.basicConfig(filename=FILE_LOG,
                    filemode='a',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)

console = logging.StreamHandler()
logging.getLogger('').addHandler(console)

request_session = requests.Session()


def setup_rest_headers():
    request_session.headers.update({'Authorization': f'Bearer {TOKEN}'})
    request_session.headers.update({'Content-Type': 'application/json'})


def get_repo_alertdefinitions():
    logging.info(f'Getting Repository alert definitions')
    files = glob.glob(FOLDER + '/**/*.json', recursive=True)
    repo_alert_list = []
    for detection_file in files:
        repo_alert_file = open(detection_file, 'r')
        repo_alert_dict = json.load(repo_alert_file)
        repo_alert_list.append(repo_alert_dict)

    return repo_alert_list


def get_devo_alertdefinitions():
    logging.info(f'Connecting to Devo for alert definitions')
    setup_rest_headers()
    alert_definitions = request_session.get(HOST + '/alerts/v1/alertDefinitions?size=1000')
    if alert_definitions.status_code == 200:
        return alert_definitions.json()
    else:
        return

def search(name_object, value_object, list_object):
    return [element for element in list_object if element[name_object] == value_object]


def send_tines_alert(jsonalert):
    url = TINES_WEBHOOK
    response = requests.post(url, json=jsonalert)
    print(jsonalert)


if __name__ == '__main__':
    repo_alert_list = get_repo_alertdefinitions()
    devo_alert_list = get_devo_alertdefinitions()

    repo_missing_queries = []
    devo_missing_queries = []


    # iterate and check if devo alert name exists in the repository
    logging.info(f'Checking if Devo Alert exists in GitLab Repository')
    for devo_alert in devo_alert_list:
        res = search('name', devo_alert["name"], repo_alert_list)
        if len(res) != 1:
            logging.warning(f'Error `{devo_alert["name"]}` does not exist in the GitLab repository')
            # Remove the system check detections
            if 'Inactivity Alert - ' not in devo_alert["name"] and 'T&S_' not in devo_alert["name"]:
                repo_missing_queries.append(f'{devo_alert["name"]}')

    # iterate and check if repo alert name exists in devo
    logging.info(f'Checking if GitLab Alert exists in Devo Alert Definitions')
    for repo_alert in repo_alert_list:
        res = search('name', repo_alert["name"], devo_alert_list)
        if len(res) != 1:
            logging.warning(f'Error `{repo_alert["name"]}` does not exist in the Devo Alert Definitions')
            devo_missing_queries.append(f'{repo_alert["name"]}')

    logging.info(f'Comparison Completed')

    complete_list = {"repo_missing_queries": repo_missing_queries, "devo_missing_queries": devo_missing_queries}
    send_tines_alert(complete_list)

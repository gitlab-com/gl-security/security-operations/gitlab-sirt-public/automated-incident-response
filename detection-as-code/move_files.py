#!/usr/bin/env python3
import shutil
import subprocess
import os
import argparse

input_file = "./changed-files.txt"
# Read input file and print the content
with open(input_file, "r") as file:
    print(file.read())

# Checking to see if the input file contains the word "detections"
with open(input_file, "r") as file:
    if "detections" in file.read():
        print("files on detections changed")

# Now process each line in the input file
with open(input_file, "r") as file:
    for line in file:
        if "detection" in line:
            fields = line.split()
            filename = fields[1]
            if fields[0] == "M":
                print(f"Copying {filename} to ./modified_rules")
                shutil.copy(filename, "./modified_rules")
            elif fields[0] == "D":
                print("A file is deleted")
                deleted_file = os.path.join("./devo", filename)
                shutil.copy(deleted_file, "./delete_file")
            elif fields[0] == "A":
                print(f"Copying {filename} to ./new_file")
                shutil.copy(filename, "./new_file")

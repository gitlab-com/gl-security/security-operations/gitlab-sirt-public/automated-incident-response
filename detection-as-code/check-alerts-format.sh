#!/bin/bash
#set -x
## Here we are checking the structure of the detections that have been added or updated 

FILES=()

# Add files in modified_rules
for f in ./modified_rules/*
do
  FILES+=("$f")
done

# Add files in new_file
for f in ./new_file/*
do
  FILES+=("$f")
done

# Add files in delete_file
for f in ./delete_file/*
do
  FILES+=("$f")
done

# Check if array is empty
if [ ${#FILES[@]} -eq 0 ]; then
  echo "No files found in the directories."
else
  # Loop through the array and process the files
  for f in "${FILES[@]}"
  do
    if [ -f "$f" ]; then
      cat "$f"
      if ! /bin/grep -q name $f; then
        echo "Error: each alert file should have name"
        cat $f
        exit 1
      fi
      if ! /bin/grep -q message $f; then
        echo "Error: each alert file should have message"
        cat $f
        exit 1
      fi 
    
      if ! /bin/grep -q description $f; then
        echo "Error: each alert file should have description"
        cat $f
        exit 1
      fi 
      if ! /bin/grep -q subcategory $f; then
        echo "Error: each alert file should have subcategory"
        cat $f
        exit 1
      fi 
      if ! /bin/grep -q isActive $f; then
        echo "Error: each alert file should have isActive"
        cat $f
        exit 1
      fi 
      if ! /bin/grep -q alertCorrelationContext $f; then
        echo "Error: each alert file should have alertCorrelationContext"
        cat $f
        exit 1
      fi 
      if ! /bin/grep -q querySourceCode $f; then
        echo "Error: each alert file should have querySourceCode"
        cat $f
        exit 1
      fi
      if ! /bin/grep -q correlationTrigger $f; then
        echo "Error: each alert file should have correlationTrigger"
        cat $f
        exit 1
      fi
      if ! [[ "$f" =~ \.json$ ]]; then
        echo "Error: detection files need to be .json format"
        cat $f
        exit 1
      fi 
    
      cat $f | jq -r '.subcategory' > subcategory.txt
      if ! /bin/grep -qi -e "SecOps" -e "ITEng" subcategory.txt; then
        echo "subcategory field in alert json file should be SecOps"
        cat $f
        exit 1
      fi

    if ! /bin/grep -q secopslookuptable $f; then
      echo "Error: each alert file should have secopslookuptable, if it is not a SIRT alert just add secopslookuptable : \"NO_SIRT_ALERT\""
      cat $f
      exit 1
    fi
    if ! /bin/grep -q secopslookuptablerow $f; then
      echo "Error: each alert file should have secopslookuptablerow, if it is not a SIRT alert just add secopslookuptablerow : \"NO_SIRT_ALERT\""
      cat $f
      exit 1
    fi
    
      cat $f | jq -r '.description' > description.txt
    
      cat $f | jq -r '.name' > name.txt
      if ! /bin/grep -q -e SecOps_ -e "ITEng_" name.txt ; then
        echo "Name field in alert json file should starts with SecOps_ for SIRT alerts or ITEng for ITEng alerts"
        cat $f
        exit 1
      fi
      if /bin/grep -qE '\.json$' name.txt ; then
        echo "The name field should not end in .json"
        cat $f
        exit 1
      fi
    
      #echo "\n#################\n"
      cat $f | jq -r '.alertCorrelationContext'| jq -r '.querySourceCode' > query.txt
      if [[ -s query.txt ]] ; then
        printf "\n So far so good\n"
        #cat query.txt
      else
        echo "the below file content is not as expected json file"
        cat $f
        exit 1
      fi
      cat query.txt | sed -r 's/["]+/\\"/g' > newquery.txt
      if ! /bin/grep -q "as entity_" query.txt && /bin/grep -qi "SecOps" subcategory.txt; then
        echo "Error: In each query you have to define an entity e.g (select username as entity_username)"
        cat $f
        exit 1
      fi 
      
      if /bin/grep -q 'lu/' newquery.txt; 
        then
          echo "This query has lookup table"
          if ! /bin/grep -q ' group ' newquery.txt; 
            then
              cat newquery.txt| sed -e 's/select .*//g' > querylast.txt
              tr -d '\n' < querylast.txt  > newquery2.txt 
              if /bin/grep -q '.rails.' newquery2.txt;  then 
                echo -n " " >> newquery2.txt
              else
                echo -n " " >> newquery2.txt
              fi
              ###### for adding alerts context to be able to use in Tines
              if /bin/grep -q '.rails.prod' newquery2.txt;  then 
                echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> newquery2.txt
              elif /bin/grep -q '.rails.api' newquery2.txt;  then 
                echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> newquery2.txt
            elif /bin/grep -q 'gsuite.reports' newquery2.txt;  then
              echo -n " select actor_email as actor select ipAddress as srcip" >> newquery2.txt
            elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' newquery2.txt;  then
              echo -n " select data__actorEmail as actor" >> newquery2.txt
              elif /bin/grep -q 'auth.okta.' newquery2.txt; then
                echo -n " select actor_alternateId as actor select client_ipAddress as srcip" >> newquery2.txt
              elif /bin/grep -q '.gcp.cloudaudit ' newquery2.txt; then
                echo -n " select proto_payload__authentication_info__principal_email as actor select proto_payload__request_metadata__caller_ip as srcip" >> newquery2.txt
              elif /bin/grep -q '.gcp.cloudaudit.' newquery2.txt; then
                echo -n " select protoPayload__authenticationInfo__principalEmail as actor" >> newquery2.txt
              elif /bin/grep -q '.aws.cloudtrail ' newquery2.txt;  then 
                    echo -n " select sourceIPAddress as srcip" >> newquery2.txt
              fi
              ######
              #cat newquery2.txt
              echo "{\"from\": \"2h\",\"limit\": 1 ,\"to\" : \"now\", \"query\": \"$(cat newquery2.txt)\"}" > query.json
              curl -X POST https://api-us.poc.devo.com/search/query  -H "Content-Type:application/json" -H "Authorization: Bearer "$queryapi"" -d @query.json > query-result.txt
              cat query-result.txt
              if /bin/grep -q "error " query-result.txt || /bin/grep -q "Error " query-result.txt; then
                echo "please review the query\n"
                cat newquery2.txt
                cat query-result.txt
                exit 1
              fi
          else
            cat newquery.txt| sed -e 's/select .*//g' > querylast.txt
            tr -d '\n' < querylast.txt  > newquery2.txt 
            if /bin/grep -q '.rails.' newquery2.txt;  then 
              echo -n " select last(parse_json)" >> newquery2.txt
            else
              echo -n " select last(parse_json)" >> newquery2.txt
            fi
            ###### for adding alerts context to be able to use in Tines
            if /bin/grep -q '.rails.prod' newquery2.txt;  then 
              echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> newquery2.txt
            elif /bin/grep -q '.rails.api' newquery2.txt;  then 
              echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> newquery2.txt
            elif /bin/grep -q 'gsuite.reports' newquery2.txt;  then
              echo -n " select last(actor_email) as actor select last(ipAddress) as srcip" >> newquery2.txt
            elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' newquery2.txt;  then
              echo -n " select last(data__actorEmail) as actor" >> newquery2.txt
            elif /bin/grep -q 'auth.okta.' newquery2.txt; then
              echo -n " select last(actor_alternateId) as actor select last(client_ipAddress) as srcip" >> newquery2.txt
            elif /bin/grep -q '.gcp.cloudaudit ' newquery2.txt; then
              echo -n " select last(proto_payload__authentication_info__principal_email) as actor select last(proto_payload__request_metadata__caller_ip) as srcip" >> newquery2.txt
            elif /bin/grep -q '.gcp.cloudaudit.' newquery2.txt; then
              echo -n " select last(protoPayload__authenticationInfo__principalEmail) as actor" >> newquery2.txt
            elif /bin/grep -q '.aws.cloudtrail ' newquery2.txt;  then 
                    echo -n " select last(sourceIPAddress) as srcip" >> newquery2.txt
            fi
            ######
            #cat newquery2.txt
            echo "{\"from\": \"2h\",\"limit\": 1 ,\"to\" : \"now\", \"query\": \"$(cat newquery2.txt)\"}" > query.json
            curl -X POST https://api-us.poc.devo.com/search/query  -H "Content-Type:application/json" -H "Authorization: Bearer "$queryapi"" -d @query.json > query-result.txt
            cat query-result.txt
            echo "######################"
            if /bin/grep -q "error " query-result.txt || /bin/grep -q "Error " query-result.txt; then
              echo "please review the query\n"
              cat newquery2.txt
              cat query-result.txt
              exit 1
            fi
          fi
        else
          if ! /bin/grep -q ' group ' newquery.txt; then
            tr -d '\n' < newquery.txt  > querylast.txt 
            if /bin/grep -q '.rails.' querylast.txt;  then 
              echo -n " " >> querylast.txt
            else
              echo -n " " >> querylast.txt
            fi
            ###### for adding alerts context to be able to use in Tines
            if /bin/grep -q '.rails.prod' querylast.txt;  then 
              echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> querylast.txt
            elif /bin/grep -q '.rails.api' querylast.txt;  then 
              echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> querylast.txt
            elif /bin/grep -q 'gsuite.reports' querylast.txt;  then
              echo -n " select actor_email as actor select ipAddress as srcip" >> querylast.txt
            elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' querylast.txt;  then
              echo -n " select data__actorEmail as actor" >> querylast.txt
            elif /bin/grep -q 'auth.okta.' querylast.txt; then
              echo -n " select actor_alternateId as actor select client_ipAddress as srcip" >> querylast.txt
            elif /bin/grep -q '.gcp.cloudaudit ' querylast.txt; then
              echo -n " select proto_payload__authentication_info__principal_email as actor select proto_payload__request_metadata__caller_ip as srcip" >> querylast.txt
            elif /bin/grep -q '.gcp.cloudaudit.' querylast.txt; then
              echo -n " select protoPayload__authenticationInfo__principalEmail as actor" >> querylast.txt
            elif /bin/grep -q '.aws.cloudtrail ' querylast.txt;  then 
                    echo -n " select sourceIPAddress as srcip" >> querylast.txt
            fi
            ######
            echo "{\"from\": \"2h\",\"limit\": 1 ,\"to\" : \"now\", \"query\": \"$(cat querylast.txt)\"}" > query.json
            curl -X POST https://api-us.poc.devo.com/search/query  -H "Content-Type:application/json" -H "Authorization: Bearer "$queryapi"" -d @query.json > query-result.txt
            #cat query-result.txt
            if /bin/grep -q "error " query-result.txt || /bin/grep -q "Error " query-result.txt; then
              echo "please review the query"
              cat querylast.txt
              cat query-result.txt
            fi
            ######
            echo "{\"from\": \"2h\",\"limit\": 1 ,\"to\" : \"now\", \"query\": \"$(cat querylast.txt)\"}" > query.json
            curl -X POST https://api-us.poc.devo.com/search/query  -H "Content-Type:application/json" -H "Authorization: Bearer "$queryapi"" -d @query.json > query-result.txt
            #cat query-result.txt
            if /bin/grep -q "error " query-result.txt || /bin/grep -q "Error " query-result.txt; then
              echo "please review the query"
              cat querylast.txt
              cat query-result.txt
              exit 1
            fi
          else 
            tr -d '\n' < newquery.txt  > querylast.txt 
            if /bin/grep -q '.rails.' querylast.txt;  then 
              echo -n " select last(parse_json)" >> querylast.txt
            else
              echo -n " select last(parse_json)" >> querylast.txt
            fi
            ###### for adding alerts context to be able to use in Tines
            if /bin/grep -q '.rails.prod' querylast.txt;  then 
              echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> querylast.txt
            elif /bin/grep -q '.rails.api' querylast.txt;  then 
              echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> querylast.txt
            elif /bin/grep -q 'gsuite.reports' querylast.txt;  then
              echo -n " select last(actor_email) as actor select last(ipAddress) as srcip" >> querylast.txt
            elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' querylast.txt;  then
              echo -n " select last(data__actorEmail) as actor" >> querylast.txt
            elif /bin/grep -q 'auth.okta.' querylast.txt; then
              echo -n " select last(actor_alternateId) as actor select last(client_ipAddress) as srcip" >> querylast.txt
            elif /bin/grep -q '.gcp.cloudaudit ' querylast.txt; then
              echo -n " select last(proto_payload__authentication_info__principal_email) as actor select last(proto_payload__request_metadata__caller_ip) as srcip" >> querylast.txt
            elif /bin/grep -q '.gcp.cloudaudit.' querylast.txt; then
              echo -n " select last(protoPayload__authenticationInfo__principalEmail) as actor" >> querylast.txt
            elif /bin/grep -q '.aws.cloudtrail ' querylast.txt;  then 
              echo -n " select last(sourceIPAddress) as srcip" >> querylast.txt
            fi
            ######
            echo "{\"from\": \"2h\",\"limit\": 1 ,\"to\" : \"now\", \"query\": \"$(cat querylast.txt)\"}" > query.json
            curl -X POST https://api-us.poc.devo.com/search/query  -H "Content-Type:application/json" -H "Authorization: Bearer "$queryapi"" -d @query.json > query-result.txt
            #cat query-result.txt
            if /bin/grep -q "error " query-result.txt || /bin/grep -q "Error " query-result.txt; then
              echo "please review the query"
              cat querylast.txt
              cat query-result.txt
              exit 1
            fi
          fi
      fi

      echo -n "" > querylast.txt
      echo -n "" > newquery.txt
      echo -n "" > newquery2.txt
    
    fi
  done
fi

######"Checking names to make sure it is unique stop being failed after merge
if ! [ -z "$(ls -A ./new_file)" ]; then
    NewFILES="./new_file/*"
    echo "Checking names to make sure it is unique"
    #cat $NewFILES 
    for newfile in $NewFILES
        do
          if [[ "$newfile" =~ \.json$ ]]; then
            cat $newfile| jq -r '.name' > name.txt
            curl -X GET "https://api-us.poc.devo.com/alerts/v1/alertDefinitions?page=0&size=1000&nameFilter="$(cat name.txt)"" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" > alertsindevo.txt
            cat alertsindevo.txt | jq '.[] | select(.name=='\"$(cat name.txt)\"')' > alertnameindevo.txt
            echo "alert name is: "
            cat name.txt
            echo "if there is any alert with same name it is :"
            cat alertnameindevo.txt
            if [ -s alertnameindevo.txt ]; then # file is not empty
              echo $newfile
              echo "alert name already exists in devo:"
              cat name.txt
                 exit 1
            fi
          fi
        done
fi

#####"Checking names to make sure the name is not being changed 

if ! [ -z "$(ls -A ./modified_rules)" ]; then
    MODIFFILES="./modified_rules/*"
    echo "Checking .... alert name"
    #cat $MODIFFILES 
    for eachfile in $MODIFFILES
        do
          if [[ "$eachfile" =~ \.json$ ]]; then
            cat $eachfile| jq -r '.name' > name.txt
            cat $eachfile| jq -r '.subcategory' > newalertsubcategory.txt
            curl -X GET "https://api-us.poc.devo.com/alerts/v1/alertDefinitions?page=0&size=1000&nameFilter="$(cat name.txt)"" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" > result_alert.txt
            cat  result_alert.txt
            cat result_alert.txt | jq '.[] | select(.name=='\"$(cat name.txt)\"')' > thealertindevo.txt
            cat thealertindevo.txt | jq -r '.subcategory' > alertsubcategoryindevo.txt
            cat thealertindevo.txt | jq -r '.name' > alertnameindevo.txt
            basename=$(basename "$(cat alertsubcategoryindevo.txt)")
            category_of_alert_in_Devo="${basename##*.}"
            echo "alertnameindevo:"
            cat alertnameindevo.txt
            echo "name.txt:"
            cat name.txt
            echo "category_of_alert_in_Devo:"
            echo "$category_of_alert_in_Devo"
            echo "newalertsubcategory.txt:"
            cat newalertsubcategory.txt


          fi
        done
fi



if  ! [ -z "$(ls -A ./delete_file)"  ]; then
    DelIFFILES="./delete_file/*"
    echo "Checking .... alert name"
    for eachfile in $DelIFFILES
        do
          if [[ "$eachfile" =~ \.json$ ]]; then
            cat $eachfile| jq -r '.name' > name.txt
            echo "searching for alert name : "
            cat name.txt
            curl -X GET "https://api-us.poc.devo.com/alerts/v1/alertDefinitions?page=0&size=1000&nameFilter="$(cat name.txt)"" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" > deletedalertindevo.txt
            cat deletedalertindevo.txt | jq '.[] | select(.name=='\"$(cat name.txt)\"')'|  jq -r '.id'  > deletedalertid.txt
            cat deletedalertindevo.txt | jq '.[] | select(.name=='\"$(cat name.txt)\"')'|  jq -r '.name'  > deletedalertname.txt
            echo "deleted_alert_id:"
            cat deletedalertid.txt
            echo "deleted_alert_name:"
            cat deletedalertname.txt
            ### Checking to make sure the ID is valid
            pattern="^[0-9]+$"
            id=$(<deletedalertid.txt)
            # Remove leading and trailing whitespace from the alert ID
            id=${id##*( )}
            id=${id%%*( )}
            if ! [[ -z $id ]] && [[ $id =~ $pattern ]]; then
              echo "all good for the MR to be merged"
            else
              echo "We could not find the ID in Devo alerts"
              #exit 1
            fi
          fi
        done
fi


            

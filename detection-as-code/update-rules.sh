#!/bin/bash
echo "modified_rules"
ls -1 ./modified_rules
echo "delete_file"
ls -1 ./delete_file 
echo "new_file"
ls -l ./new_file

if [ -z "$(ls -A ./modified_rules)" ]; then
    echo "modified_rules is empty"
else
    FILES="./modified_rules/*"
    for file in $FILES
        do
          echo $file
          if [[ "$file" =~ \.json$ ]]; then
            cp $file alert.json
            cat $file| jq -r '.name' > name.txt
            curl -X GET "https://api-us.poc.devo.com/alerts/v1/alertDefinitions?page=0&size=1000&nameFilter="$(cat name.txt)"" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" > result_alert.txt
            cat result_alert.txt | jq '.[] | select(.name=='\"$(cat name.txt)\"')'|  jq -r '.id'  > id.txt
            cat alert.json | jq ".+= {"id":\"$(cat id.txt)\"}" > alert1.json
            ############
            cat alert1.json | jq -r '.alertCorrelationContext'| jq -r '.querySourceCode' > query.txt
            tr -d '\n' < query.txt  > querylast.txt 
            cat $file | jq -r '.secopslookuptable' > secopslookuptable.txt
            cat $file | jq -r '.secopslookuptablerow' > secopslookuptablerow.txt
            cat $file | jq -r '.subcategory' > subcategory.txt
            if /bin/grep -qi -e "SecOps" subcategory.txt; then
              echo -n " select \`lu/"$(cat secopslookuptable.txt)"/alertType\`('"$(cat secopslookuptablerow.txt)"') as alertType,\`lu/"$(cat secopslookuptable.txt)"/UAM\`('"$(cat secopslookuptablerow.txt)"') as UAM, \`lu/"$(cat secopslookuptable.txt)"/alertMitreTactics\`('"$(cat secopslookuptablerow.txt)"') as alertMitreTactics,\`lu/"$(cat secopslookuptable.txt)"/alertMitreTechniques\`('"$(cat secopslookuptablerow.txt)"') as alertMitreTechniques,\`lu/"$(cat secopslookuptable.txt)"/alertPriority\`('"$(cat secopslookuptablerow.txt)"') as alertPriority" >> querylast.txt
            fi
            if ! /bin/grep -q ' group ' querylast.txt; then 
              if /bin/grep -q '.rails.' querylast.txt;  then 
                echo -n " select parsed_json" >> querylast.txt
              else
                echo -n " select parsed_json" >> querylast.txt
              fi
              ###### for adding alerts context to be able to use in Tines
              if /bin/grep -q '.rails.prod' querylast.txt;  then 
                echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> querylast.txt
              elif /bin/grep -q '.rails.api' querylast.txt;  then 
                echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> querylast.txt
              elif /bin/grep -q '.gsuite.reports' querylast.txt;  then
                echo -n " select actor_email as actor select ipAddress as srcip" >> querylast.txt
              elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' querylast.txt;  then
                echo -n " select data__actorEmail as actor" >> querylast.txt
              elif /bin/grep -q 'auth.okta.' querylast.txt; then
                echo -n " select actor_alternateId as actor select client_ipAddress as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit ' querylast.txt; then
                echo -n " select proto_payload__authentication_info__principal_email as actor select proto_payload__request_metadata__caller_ip as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit.' querylast.txt; then
                echo -n " select protoPayload__authenticationInfo__principalEmail as actor" >> querylast.txt
              elif /bin/grep -q '.aws.cloudtrail ' querylast.txt;  then 
                echo -n " select sourceIPAddress as srcip" >> querylast.txt
              fi
              ######
            else
              if /bin/grep -q '.rails.' querylast.txt;  then 
                echo -n " select last(parsed_json)" >> querylast.txt
              else
                echo -n " select last(parsed_json)" >> querylast.txt
              fi
              ###### for adding alerts context to be able to use in Tines
              if /bin/grep -q '.rails.prod' querylast.txt;  then 
                echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> querylast.txt
              elif /bin/grep -q '.rails.api' querylast.txt;  then 
                echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> querylast.txt
              elif /bin/grep -q '.gsuite.reports' querylast.txt;  then
                echo -n " select last(actor_email) as actor select last(ipAddress) as srcip" >> querylast.txt
              elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' querylast.txt;  then
                echo -n " select last(data__actorEmail) as actor" >> querylast.txt
              elif /bin/grep -q 'auth.okta.' querylast.txt; then
                echo -n " select last(actor_alternateId) as actor select last(client_ipAddress) as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit ' querylast.txt; then
                echo -n " select last(proto_payload__authentication_info__principal_email) as actor select last(proto_payload__request_metadata__caller_ip) as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit.' querylast.txt; then
                echo -n " select last(protoPayload__authenticationInfo__principalEmail) as actor" >> querylast.txt
              elif /bin/grep -q '.aws.cloudtrail ' querylast.txt;  then 
                echo -n " select last(sourceIPAddress) as srcip" >> querylast.txt
              fi
              ######
            fi
            #cat querylast.txt
            cat alert1.json | jq 'del(.alertCorrelationContext.querySourceCode)' > deletequerySourceCode.json
            cat deletequerySourceCode.json | jq 'del(.secopslookuptable)' > deletesecopslookuptable.json
            cat deletesecopslookuptable.json | jq 'del(.secopslookuptablerow)' > deletesecopslookuptablerow.json
            cat deletesecopslookuptablerow.json | jq 'del(.comment)' > deletecomment.json
            cat deletecomment.json | jq ".alertCorrelationContext+= {"querySourceCode":\"$(cat querylast.txt)\"}" > newjsonfile.json
            echo "**************************"
            cat newjsonfile.json 
            echo "**************************"
            ###########
            curl -X PUT "https://api-us.poc.devo.com/alerts/v1/alertDefinitions" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" -d @newjsonfile.json
          fi
        done
fi


if [ -z "$(ls -A ./new_file)" ]; then
    echo "new_file is empty"
else
    FILES="./new_file/*"
    for file in $FILES
        do
          echo "##########"
          echo $file
          if [[ "$file" =~ \.json$ ]]; then
            cat $file | jq -r '.alertCorrelationContext'| jq -r '.querySourceCode' > query.txt
            tr -d '\n' < query.txt  > querylast.txt 
            cat $file | jq -r '.secopslookuptable' > secopslookuptable.txt
            cat $file | jq -r '.secopslookuptablerow' > secopslookuptablerow.txt
            cat $file | jq -r '.subcategory' > subcategory.txt
            if /bin/grep -qi -e "SecOps" subcategory.txt; then
              echo -n " select \`lu/"$(cat secopslookuptable.txt)"/alertType\`('"$(cat secopslookuptablerow.txt)"') as alertType,\`lu/"$(cat secopslookuptable.txt)"/UAM\`('"$(cat secopslookuptablerow.txt)"') as UAM, \`lu/"$(cat secopslookuptable.txt)"/alertMitreTactics\`('"$(cat secopslookuptablerow.txt)"') as alertMitreTactics,\`lu/"$(cat secopslookuptable.txt)"/alertMitreTechniques\`('"$(cat secopslookuptablerow.txt)"') as alertMitreTechniques,\`lu/"$(cat secopslookuptable.txt)"/alertPriority\`('"$(cat secopslookuptablerow.txt)"') as alertPriority" >> querylast.txt
            fi
            if ! /bin/grep -q ' group ' querylast.txt; then 
              ###### for adding alerts context to be able to use in Tines
              if /bin/grep -q '.rails.prod' querylast.txt;  then 
                echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> querylast.txt
              elif /bin/grep -q '.rails.api' querylast.txt;  then 
                echo -n " select user_id as actor select meta_remote_ip as srcip select ua as USERAGENT" >> querylast.txt                
              elif /bin/grep -q '.gsuite.reports' querylast.txt;  then
                echo -n " select actor_email as actor select ipAddress as srcip" >> querylast.txt
              elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' querylast.txt;  then
                echo -n " select data__actorEmail as actor" >> querylast.txt
              elif /bin/grep -q 'auth.okta.' querylast.txt; then
                echo -n " select actor_alternateId as actor select client_ipAddress as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit ' querylast.txt; then
                echo -n " select proto_payload__authentication_info__principal_email as actor select proto_payload__request_metadata__caller_ip as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit.' querylast.txt; then
                echo -n " select protoPayload__authenticationInfo__principalEmail as actor" >> querylast.txt
              elif /bin/grep -q '.aws.cloudtrail ' querylast.txt;  then 
                echo -n " select sourceIPAddress as srcip" >> querylast.txt
              fi
              ######
             else
            ###### for adding alerts context to be able to use in Tines
              if /bin/grep -q '.rails.prod' querylast.txt;  then 
                echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> querylast.txt
              elif /bin/grep -q '.rails.api' querylast.txt;  then 
                echo -n " select last(user_id) as actor select last(meta_remote_ip) as srcip select last(ua) as USERAGENT" >> querylast.txt 
              elif /bin/grep -q '.gsuite.reports' querylast.txt;  then
                echo -n " select last(actor_email) as actor select last(ipAddress) as srcip" >> querylast.txt
              elif /bin/grep -q '.gsuite.alerts.super_admin_password_reset' querylast.txt;  then
                echo -n " select last(data__actorEmail) as actor" >> querylast.txt
              elif /bin/grep -q 'auth.okta.' querylast.txt; then
                echo -n " select last(actor_alternateId) as actor select last(client_ipAddress) as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit ' querylast.txt; then
                echo -n " select last(proto_payload__authentication_info__principal_email) as actor select last(proto_payload__request_metadata__caller_ip) as srcip" >> querylast.txt
              elif /bin/grep -q '.gcp.cloudaudit.' querylast.txt; then
                echo -n " select last(protoPayload__authenticationInfo__principalEmail) as actor" >> querylast.txt
              elif /bin/grep -q '.aws.cloudtrail ' querylast.txt;  then 
                echo -n " select last(sourceIPAddress) as srcip" >> querylast.txt
              fi
              ######
            fi
            cat querylast.txt 
            cat $file | jq 'del(.alertCorrelationContext.querySourceCode)' > deletequerySourceCode.json
            cat deletequerySourceCode.json | jq 'del(.secopslookuptable)' > deletesecopslookuptable.json
            cat deletesecopslookuptable.json | jq 'del(.secopslookuptablerow)' > deletesecopslookuptablerow.json
            cat deletesecopslookuptablerow.json | jq 'del(.comment)' > deletecomment.json
            cat deletecomment.json | jq ".alertCorrelationContext+= {"querySourceCode":\"$(cat querylast.txt)\"}" > newjsonfile.json
            echo "**************************"
            cat newjsonfile.json 
            echo "**************************"
            cp newjsonfile.json alert.json
            curl -X POST https://api-us.poc.devo.com/alerts/v1/alertDefinitions  -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" -d @alert.json > result_new_alert.txt
            cat result_new_alert.txt | jq . 
              if /bin/grep -q error result_new_alert.txt; then
                cat query.json
                cat result_new_alert.txt
              fi 
          fi
        done
fi

if  ! [ -z "$(ls -A ./delete_file)"  ]; then
    DelIFFILES="./delete_file/*"
    echo "Checking .... alert name"
    for eachfile in $DelIFFILES
        do
          if [[ "$eachfile" =~ \.json$ ]]; then
            cat $eachfile| jq -r '.name' > name.txt
            echo "searching for alert name : "
            cat name.txt
            echo "...."
            curl -X GET "https://api-us.poc.devo.com/alerts/v1/alertDefinitions?page=0&size=1000&nameFilter="$(cat name.txt)"" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" > deletedalertindevo.txt
            cat deletedalertindevo.txt | jq '.[] | select(.name=='\"$(cat name.txt)\"')'|  jq -r '.id'  > deletedalertid.txt
            cat deletedalertindevo.txt | jq '.[] | select(.name=='\"$(cat name.txt)\"')'|  jq -r '.name'  > deletedalertname.txt
            echo "deleted_alert_id:"
            cat deletedalertid.txt
            echo "deleted_alert_name:"
            cat deletedalertname.txt
            pattern="^[0-9]+$"
            id=$(<deletedalertid.txt)
            # Remove leading and trailing whitespace from the alert ID
            id=${id##*( )}
            id=${id%%*( )}
            if ! [[ -z $id ]] && [[ $id =~ $pattern ]]; then
              curl -X GET "https://api-us.poc.devo.comalerts/v1/alertDefinitions?idFilter="$(cat deletedalertid.txt)"" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" > deletedalertdetails.txt
              curl -X DELETE "https://api-us.poc.devo.comalerts/v1/alertDefinitions?alertIds="$(cat deletedalertid.txt)"" -H "Content-Type:application/json" -H "Authorization: Bearer "$token"" > deletedalertdetails.txt              
              cat deletedalertdetails.txt
            else
              echo "We could not find the ID in Devo alerts"
              #exit 1
            fi
            
          fi
        done
fi
